variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}
variable "private_key_path" {}
variable "key_name" {
  default = "PluralsightKeys"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

resource "aws_instance" "nginx" {
  ami           = "ami-0dcf15c37a41c965f"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"

  connection {
    user = "ubuntu"
    private_key = "${file(var.private_key_path)}"
    host = "${aws_instance.nginx.public_ip}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt --assume-yes update",
      "sudo apt --assume-yes install apache2"
    ]
  }
}

output "aws_instance_public_ip" {
  value = "${aws_instance.nginx.public_ip}"
}
