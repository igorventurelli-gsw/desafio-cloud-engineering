# Desafio Quero Educação - Cloud Engineering

### Como entregar estes desafios
Você deve realizar o fork deste projeto e fazer o push no seu próprio repositório e enviar o link como resposta ao recrutador que lhe enviou o teste, junto com seu LinkedIn atualizado.

A implementação deve ficar na pasta correspondente ao desafio. Fique à vontade para adicionar qualquer tipo de conteúdo que julgue útil ao projeto, alterar/acrescentar um README com instruções de como executá-lo, etc.

### Extras
- Descreva o processo de resolução dos desafios;
- Descreva como utilizar a sua solução;
- Sempre considerar melhores práticas como se fosse um ambiente de produção;

### Prazo
Você tem 5 dias após o recebimento do desafio para completa-lo.

#### Boa sorte 
